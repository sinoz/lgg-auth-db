# Latency.GG authentication and authorization database
Where we store our auth stuff!

## Everything is a patch
We think of a database as a series of patches, applied in a specific order. The result of those patches is the schema of the database.

Those patches live in the `src/db/patch` folder. Patches can be made dependent on other patches via the `-- @require <name-of-patch>` directive. When the patches are compiled to one big patch, they are first dependency sorted by this directive. The name of the patch is the basename of the file, so without any extension or directory name.

Example
table.sql:
```sql
CREATE TABLE public.t (
    id integer NOT NULL
);
```

pk.sql:
```sql
-- @require table

ALTER TABLE public.t
ADD CONSTRAINT t_pk PRIMARY KEY (id);
```

In this example `table.sql` will get executed before `ok.sql`.

The patch script can be built via `make out/patch.sql`. The resulting `patch.sql` file contains all patches in the right order. This script may be executed many times on the same database, there is logic in the script that will make sure every patch is applied exactly once.

When deploying the database this script is executed in a transaction.

By running `make out/schema.sql` a file is created that contains the entire schema, and is supposed to be run once on an ampty database.

## Rolling up
The number of patches will keep growing as we develop the database. This will eventually lead to an incredible amount of patches sitting in the `src/db/patch` folder. This can get messsy very quick.

That is why we can rollup the pathes into the `src/db/snapshot.sql` file. This file is like a snapshot of a database with all patches applied. We can create the rollup via `make rollup`.

After the rollup, and after all patches are deployed, the patches may be removed. We can still genereate the database by running `/out/schema.sql`, but our `src/db/patch` folder is clean and under control.

Do note that when we delete a patch, the patch has to be aplied to the production (and any other) database! If we delete the patch before it is applied we lose the ability yo migrate the database.

## Deployment
Deployment can be done by tagging the source repository with a tag that starts with a `v`, and then a semver compatible version. For instance `v1.0.0`.

A semver production release will release to production, a prerelease will release to the specified prerelease environment, for instance `v1.0.0-alpha` will release to our alpha environment.

A docker image is alsop built and deployed for every tag that starts with a `v`. This docker image can be used for testing and development.

An npm package is published on every release. This package exposes the contents of the `schema.sql` file and can be used for testing purpose in node applications.

