SHELL:=$(PREFIX)/bin/sh
TS_SRC:=$(wildcard src/*.ts)
JS_OUT:=$(patsubst src/%.ts,out/%.js,$(TS_SRC))
DTS_OUT:=$(patsubst src/%.ts,out/%.d.ts,$(TS_SRC))

build: \
	out/schema.sql \
	out/patch.sql \
	$(JS_OUT) \
	$(DTS_OUT) \
	
rebuild: clean build
clean:
	rm -rf out

repl: out/schema.sql
	@dropdb --if-exists "repl"
	createdb "repl"
	for F in $^; do psql --dbname "repl" --username postgres --file $$F; done
	psql --dbname "repl"
	dropdb --if-exists "repl"

rollup: \
	out/schema.sql \

	@dropdb --if-exists "rollup"
	createdb "rollup"
	for F in $^; do psql --dbname "rollup" --username postgres --file $$F; done
	pg_dump --dbname "rollup" --username postgres --column-inserts --disable-triggers --file src/db/snapshot.sql
	dropdb --if-exists "rollup"
	rm src/db/patch/*.sql
	echo -e 'truncate table sqlpatch.patch;\n' > src/db/patch/initial.sql


out/%.sql: src/db/%/*.sql
	@mkdir -p $(@D)
	
	$(shell npm bin)/sqlpatch --table=$* $^ > $@

out/schema.sql: src/db/snapshot.sql out/patch.sql
	@mkdir -p $(@D)
	cat $^ > $@

$(JS_OUT) $(DTS_OUT): $(TS_SRC)
	$(shell npm bin)/tsc

.PHONY: \
	build \
	rebuild \
	clean \
	repl \
	rollup \
