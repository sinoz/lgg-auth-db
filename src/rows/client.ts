import pg from "pg";
import { RowDescriptor, RowFilter } from "table-access";
import { createRowEvents, getTriggerEventsFanoutMemoizer, RowEventUnion } from "../utils/index.js";

const schema = "public";
const table = "client";

export interface ClientRowKey {
    id: string
}
export interface ClientRowData {
    name: string
    salt: string | null
    created_utc: string
}
export type ClientRow = ClientRowKey & ClientRowData
export const clientRowDescriptor: RowDescriptor<ClientRow> = {
    schema, table,
};
export type ClientRowEvent = RowEventUnion<ClientRowKey, ClientRowData>;

export function createClientRowEvents(
    pool: pg.Pool,
    filter: Partial<ClientRowKey> | RowFilter<ClientRowKey>,
    signal: AbortSignal,
): AsyncIterable<ClientRowEvent> {
    const rowEventsFanoutMemoizer = getTriggerEventsFanoutMemoizer(pool);

    return createRowEvents<ClientRowKey, ClientRowData>({
        channel: "row-event",
        schema,
        table,
        filter,
        pool,
        signal,
        triggerEventsFanoutMemoizer: rowEventsFanoutMemoizer,
    });
}
