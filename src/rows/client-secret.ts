import pg from "pg";
import { RowDescriptor, RowFilter } from "table-access";
import { createRowEvents, getTriggerEventsFanoutMemoizer, RowEventUnion } from "../utils/index.js";

const schema = "public";
const table = "client_secret";

export interface ClientSecretRowKey {
    client_id: string
    digest: string
}
export interface ClientSecretRowData {
    created_utc: string
}
export type ClientSecretRow = ClientSecretRowKey & ClientSecretRowData;
export const clientSecretRowDescriptor: RowDescriptor<ClientSecretRow> = {
    schema, table,
};
export type ClientSecretRowEvent = RowEventUnion<ClientSecretRowKey, ClientSecretRowData>;

export function createClientSecretRowEvents(
    pool: pg.Pool,
    filter: Partial<ClientSecretRowKey> | RowFilter<ClientSecretRowKey>,
    signal: AbortSignal,
): AsyncIterable<ClientSecretRowEvent> {
    const rowEventsFanoutMemoizer = getTriggerEventsFanoutMemoizer(pool);

    return createRowEvents<ClientSecretRowKey, ClientSecretRowData>({
        channel: "row-event",
        schema,
        table,
        filter,
        pool,
        signal,
        triggerEventsFanoutMemoizer: rowEventsFanoutMemoizer,
    });
}
