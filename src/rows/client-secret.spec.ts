import assert from "assert";
import { queryDelete, queryInsert, withTransaction } from "table-access";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";
import { clientSecretRowDescriptor, createClientSecretRowEvents } from "./client-secret.js";

test("client-secret", t => withContext(async context => {
    const abortController = new AbortController();
    try {
        const eventIterable = createClientSecretRowEvents(context.pool, {}, abortController.signal);
        const eventIterator = eventIterable[Symbol.asyncIterator]();

        try {
            {
                const eventNext = await eventIterator.next();
                assert(eventNext.done !== true);
                assert(eventNext.value.type === "snapshot");
                t.deepEqual(
                    eventNext.value.rows,
                    [
                        {
                            client_id: "\\x31",
                            digest: "\\x9120cd5faef07a08e971ff024a3fcbea1e3a6b44142a6d82ca28c6c42e4f852595bcf53d81d776f10541045abdb7c37950629415d0dc66c8d86c64a5606d32de",
                            created_utc: "2021-11-22T11:24:40",
                        },
                    ],
                );
            }

            await withTransaction(context.pool, async client => {
                await queryInsert(client,
                    clientSecretRowDescriptor,
                    {

                        client_id: "\\x31",
                        digest: "\\x33",
                        created_utc: "2022-03-26T08:04:19",
                    },
                );
            });

            {
                const eventNext = await eventIterator.next();
                assert(eventNext.done !== true);
                assert(eventNext.value.type === "insert");
                t.deepEqual(
                    eventNext.value.key,
                    {
                        client_id: "\\x31",
                        digest: "\\x33",
                    },
                );
                t.deepEqual(
                    eventNext.value.data,
                    {
                        created_utc: "2022-03-26T08:04:19",
                    },
                );
            }

            await withTransaction(context.pool, async client => {
                await queryDelete(client,
                    clientSecretRowDescriptor,
                    {
                        client_id: "\\x31",
                        digest: "\\x33",
                    },
                );
            });

            {
                const eventNext = await eventIterator.next();
                assert(eventNext.done !== true);
                assert(eventNext.value.type === "delete");
                t.deepEqual(
                    eventNext.value.key,
                    {
                        client_id: "\\x31",
                        digest: "\\x33",
                    },
                );
            }
        }
        finally {
            t.comment("aborting");
            abortController.abort();

            const eventNext = await eventIterator.next();
            assert(eventNext.done === true);

            t.comment("aborted");
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }

}));
