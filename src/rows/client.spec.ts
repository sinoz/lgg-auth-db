import assert from "assert";
import { queryDelete, queryInsert, queryUpdate, withTransaction } from "table-access";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";
import { clientRowDescriptor, createClientRowEvents } from "./client.js";

test("client", t => withContext(async context => {
    const abortController = new AbortController();
    try {
        const eventIterable = createClientRowEvents(context.pool, {}, abortController.signal);
        const eventIterator = eventIterable[Symbol.asyncIterator]();

        try {
            {
                const eventNext = await eventIterator.next();
                assert(eventNext.done !== true);
                assert(eventNext.value.type == "snapshot");
                t.deepEqual(
                    eventNext.value.rows,
                    [
                        {
                            id: "\\x31",
                            name: "testing",
                            salt: "\\x",
                            created_utc: "2021-11-22T11:24:40",
                        },
                    ],
                );
            }

            await withTransaction(context.pool, async client => {
                await queryInsert(client,
                    clientRowDescriptor,
                    {
                        id: "\\x32",
                        name: "testing",
                        salt: "\\x33",
                        created_utc: "2022-03-26T08:04:19",
                    },
                );
            });

            {
                const eventNext = await eventIterator.next();
                assert(eventNext.done !== true);
                assert(eventNext.value.type == "insert");
                t.deepEqual(
                    eventNext.value.key,
                    {
                        id: "\\x32",
                    },
                );
                t.deepEqual(
                    eventNext.value.data,
                    {
                        name: "testing",
                        salt: "\\x33",
                        created_utc: "2022-03-26T08:04:19",
                    },
                );
            }

            await withTransaction(context.pool, async client => {
                await queryUpdate(client,
                    clientRowDescriptor,
                    {
                        id: "\\x32",
                    },
                    {
                        name: "testing-123",
                    },
                );
            });

            {
                const eventNext = await eventIterator.next();
                assert(eventNext.done !== true);
                assert(eventNext.value.type == "update");
                t.deepEqual(
                    eventNext.value.key,
                    {
                        id: "\\x32",
                    },
                );
                t.deepEqual(
                    eventNext.value.data,
                    {
                        name: "testing-123",
                        salt: "\\x33",
                        created_utc: "2022-03-26T08:04:19",
                    },
                );
            }

            await withTransaction(context.pool, async client => {
                await queryDelete(client,
                    clientRowDescriptor,
                    {
                        id: "\\x32",
                    },
                );
            });

            {
                const eventNext = await eventIterator.next();
                assert(eventNext.done !== true);
                assert(eventNext.value.type == "delete");
                t.deepEqual(
                    eventNext.value.key,
                    {
                        id: "\\x32",
                    },
                );
            }
        }
        finally {
            t.comment("aborting");
            abortController.abort();

            const eventNext = await eventIterator.next();
            assert(eventNext.done === true);

            t.comment("aborted");
        }
    }
    catch (error) {
        if (!abortController.signal.aborted) throw error;
    }
}));
