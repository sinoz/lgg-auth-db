--
-- PostgreSQL database dump
--

-- Dumped from database version 13.4 (Debian 13.4-1.pgdg100+1)
-- Dumped by pg_dump version 13.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sqlpatch; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sqlpatch;


ALTER SCHEMA sqlpatch OWNER TO postgres;

SET default_tablespace = '';

-- SET default_table_access_method = heap;

--
-- Name: access_token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.access_token (
    client_id bytea NOT NULL,
    user_id bytea,
    digest bytea NOT NULL,
    created_utc timestamp without time zone NOT NULL,
    expires_utc timestamp without time zone NOT NULL
);


ALTER TABLE public.access_token OWNER TO postgres;

--
-- Name: authorization_code; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.authorization_code (
    client_id bytea NOT NULL,
    user_id bytea,
    digest bytea NOT NULL,
    created_utc timestamp without time zone NOT NULL,
    expires_utc timestamp without time zone NOT NULL
);


ALTER TABLE public.authorization_code OWNER TO postgres;

--
-- Name: client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client (
    id bytea NOT NULL,
    name character varying(100) NOT NULL,
    salt bytea NOT NULL,
    created_utc timestamp without time zone NOT NULL
);


ALTER TABLE public.client OWNER TO postgres;

--
-- Name: client_secret; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.client_secret (
    client_id bytea NOT NULL,
    digest bytea NOT NULL,
    created_utc timestamp without time zone NOT NULL
);


ALTER TABLE public.client_secret OWNER TO postgres;

--
-- Name: redirect_uri; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.redirect_uri (
    client_id bytea NOT NULL,
    value character varying NOT NULL,
    created_utc timestamp without time zone NOT NULL
);


ALTER TABLE public.redirect_uri OWNER TO postgres;

--
-- Name: refresh_token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.refresh_token (
    client_id bytea NOT NULL,
    user_id bytea,
    digest bytea NOT NULL,
    created_utc timestamp without time zone NOT NULL,
    expires_utc timestamp without time zone NOT NULL
);


ALTER TABLE public.refresh_token OWNER TO postgres;

--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id bytea NOT NULL,
    name character varying(100) NOT NULL,
    salt bytea NOT NULL,
    created_utc timestamp without time zone NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_password; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_password (
    user_id bytea NOT NULL,
    digest bytea NOT NULL,
    created_utc timestamp without time zone NOT NULL
);


ALTER TABLE public.user_password OWNER TO postgres;

--
-- Name: patch; Type: TABLE; Schema: sqlpatch; Owner: postgres
--

CREATE TABLE sqlpatch.patch (
    name character varying(100) NOT NULL,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    checksum character(40)
);


ALTER TABLE sqlpatch.patch OWNER TO postgres;

--
-- Data for Name: access_token; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: authorization_code; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.client (id, name, salt, created_utc) VALUES ('\x31', 'testing', '\x', '2021-11-22 11:24:40');


--
-- Data for Name: client_secret; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.client_secret (client_id, digest, created_utc) VALUES ('\x31', '\x9120cd5faef07a08e971ff024a3fcbea1e3a6b44142a6d82ca28c6c42e4f852595bcf53d81d776f10541045abdb7c37950629415d0dc66c8d86c64a5606d32de', '2021-11-22 11:24:40');


--
-- Data for Name: redirect_uri; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: refresh_token; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: user_password; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: patch; Type: TABLE DATA; Schema: sqlpatch; Owner: postgres
--

INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('user', '2021-12-20 12:24:19.207823', '210c332c36d41d3d6eb4a73f37cda5f417845a3e');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('client', '2021-12-20 12:24:19.211846', 'c6e5d4f4fe6bfe43d72e80eafcaf180526913a45');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('access_token', '2021-12-20 12:24:19.214936', '587627ce4710b8496914143f1683168527e40b3a');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('authorization_code', '2021-12-20 12:24:19.219497', '19b3fbfe9487be24164083c540a8f5db090d9f0c');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('client_secret', '2021-12-20 12:24:19.223224', '78fe0ddff1b27464e9fb0a2fe3f22ff05c2cb6ac');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('redirect_uri', '2021-12-20 12:24:19.226691', '5d4fe2e960f8ab3d32b1ea836fdfeb897bd558ae');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('refresh_token', '2021-12-20 12:24:19.229903', '66133dfc729a00bb0210063533b667ce8c4dd544');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('testing_client', '2021-12-20 12:24:19.233686', '71d1f00a82e20fb566151b7d93a82a7bdcd05af8');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('testing_client_fix', '2021-12-20 12:24:19.234886', '773896f0b8cdf6f17bc5cd661157539949ce1f51');
INSERT INTO sqlpatch.patch (name, created, checksum) VALUES ('user_password', '2021-12-20 12:24:19.235696', 'd8aa79563954f18502b7a122bd11ac971610aa35');


--
-- Name: access_token access_token_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_token
    ADD CONSTRAINT access_token_pk PRIMARY KEY (client_id, digest);


--
-- Name: authorization_code authorization_code_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authorization_code
    ADD CONSTRAINT authorization_code_pk PRIMARY KEY (client_id, digest);


--
-- Name: client client_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT client_pk PRIMARY KEY (id);


--
-- Name: client_secret client_secret_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_secret
    ADD CONSTRAINT client_secret_pk PRIMARY KEY (client_id, digest);


--
-- Name: redirect_uri redirect_uri_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redirect_uri
    ADD CONSTRAINT redirect_uri_pk PRIMARY KEY (client_id, value);


--
-- Name: refresh_token refresh_token_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refresh_token
    ADD CONSTRAINT refresh_token_pk PRIMARY KEY (client_id, digest);


--
-- Name: user_password user_password_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_password
    ADD CONSTRAINT user_password_pk PRIMARY KEY (user_id);


--
-- Name: user user_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id);


--
-- Name: patch patch_pkey; Type: CONSTRAINT; Schema: sqlpatch; Owner: postgres
--

ALTER TABLE ONLY sqlpatch.patch
    ADD CONSTRAINT patch_pkey PRIMARY KEY (name);


--
-- Name: access_token access_token_client_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_token
    ADD CONSTRAINT access_token_client_fk FOREIGN KEY (client_id) REFERENCES public.client(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: access_token access_token_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.access_token
    ADD CONSTRAINT access_token_user_fk FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: authorization_code authorization_code_client_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authorization_code
    ADD CONSTRAINT authorization_code_client_fk FOREIGN KEY (client_id) REFERENCES public.client(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: authorization_code authorization_code_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authorization_code
    ADD CONSTRAINT authorization_code_user_fk FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: client_secret client_secret_client_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.client_secret
    ADD CONSTRAINT client_secret_client_fk FOREIGN KEY (client_id) REFERENCES public.client(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: redirect_uri redirect_uri_client_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.redirect_uri
    ADD CONSTRAINT redirect_uri_client_fk FOREIGN KEY (client_id) REFERENCES public.client(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: refresh_token refresh_token_client_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refresh_token
    ADD CONSTRAINT refresh_token_client_fk FOREIGN KEY (client_id) REFERENCES public.client(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: refresh_token refresh_token_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.refresh_token
    ADD CONSTRAINT refresh_token_user_fk FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: user_password user_password_user_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_password
    ADD CONSTRAINT user_password_user_fk FOREIGN KEY (user_id) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

