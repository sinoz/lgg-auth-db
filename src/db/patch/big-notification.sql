CREATE SEQUENCE public.big_notification_seq
    AS SMALLINT
    CYCLE;

CREATE FUNCTION public.dispatch_big_notification(text, text, int default 6000)
    RETURNS VOID
    LANGUAGE plpgsql SECURITY DEFINER
AS $$
DECLARE
    notification_id INT := nextval('public.big_notification_seq');
    notification_payload ALIAS FOR $2;
    notification_length INT;
    chunk_size ALIAS FOR $3;
    chunk_count INT;
    chunk_left INT;
    chunk_payload TEXT;
    notify_channel ALIAS FOR $1;
    notify_message TEXT;
BEGIN
    notification_length = LENGTH(notification_payload);
    chunk_count := notification_length / chunk_size;

    IF mod(notification_length, chunk_size) > 0
    THEN
        chunk_count := chunk_count + 1;
    END IF;

    FOR chunk_number IN 1..chunk_count
    LOOP
        chunk_left = chunk_count - chunk_number;
        chunk_payload = SUBSTRING(
            notification_payload,
            (chunk_number - 1) * chunk_size + 1,
            chunk_size
        );
        notify_message = FORMAT(
            E'%s\n%s\n%s',
            notification_id,
            chunk_left,
            chunk_payload
        );
        PERFORM pg_notify(
            notify_channel,
            notify_message
        );
    END LOOP;
END;
$$;
