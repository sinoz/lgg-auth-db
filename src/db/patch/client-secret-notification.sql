--@require row-event

perform public.create_row_event_trigger(
    'public',
    'client_secret_row_event',
    'row-event',
    array['client_id', 'digest'],
    array['created_utc']
);

create trigger client_secret_row_event_trg_all after
insert or update or delete
on public.client_secret
for each row
execute procedure public.client_secret_row_event()
;
