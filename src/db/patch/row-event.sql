--@require big-notification

CREATE FUNCTION public.create_row_event_trigger(
    function_schema TEXT,
    function_name TEXT,
    notify_channel TEXT,
    key_fields TEXT[],
    data_fields TEXT[],
    chunk_size INT DEFAULT 6000
)
    RETURNS VOID
    LANGUAGE plpgsql
    SECURITY DEFINER
AS $$
DECLARE
    field TEXT;
    key_validation_parts TEXT[];
    key_old_parts TEXT[];
    key_new_parts TEXT[];
    data_new_parts TEXT[];
    command TEXT;
BEGIN

    FOREACH field IN ARRAY key_fields
    LOOP
        key_validation_parts := key_validation_parts || FORMAT('
IF NEW.%1$I != OLD.%1$I
THEN
    RAISE EXCEPTION ''key "%1$s" change not allowed'';
END IF;
',
            field
        );

        key_old_parts := key_old_parts || FORMAT('%L', field);
        key_old_parts := key_old_parts || FORMAT('OLD.%I', field);

        key_new_parts := key_new_parts || FORMAT('%L', field);
        key_new_parts := key_new_parts || FORMAT('NEW.%I', field);
    END LOOP;

    FOREACH field IN ARRAY data_fields
    LOOP
        data_new_parts := data_new_parts || FORMAT('%L', field);
        data_new_parts := data_new_parts || FORMAT('NEW.%I', field);
    END LOOP;

    command := FORMAT('
CREATE FUNCTION %1$I.%2$I()
    RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
AS $trigger$
DECLARE
    event_payload TEXT;
BEGIN
    %5$s

    event_payload := CASE TG_OP
        WHEN ''INSERT'' THEN json_build_object(
            ''op'', TG_OP,
            ''schema'', TG_TABLE_SCHEMA,
            ''table'', TG_TABLE_NAME,
            ''key'', json_build_object(%6$s),
            ''data'', json_build_object(%8$s) 
        )
        WHEN ''UPDATE'' THEN json_build_object(
            ''op'', TG_OP,
            ''schema'', TG_TABLE_SCHEMA,
            ''table'', TG_TABLE_NAME,
            ''key'', json_build_object(%6$s),
            ''data'', json_build_object(%8$s) 
        )
        WHEN ''DELETE'' THEN json_build_object(
            ''op'', TG_OP,
            ''schema'', TG_TABLE_SCHEMA,
            ''table'', TG_TABLE_NAME,
            ''key'', json_build_object(%7$s)
        )
    END::text;

    PERFORM public.dispatch_big_notification(
        %3$L,
        event_payload,
        %4$L
    );

    RETURN NEW;
END;
$trigger$;
',
        function_schema,
        function_name,
        notify_channel,
        chunk_size,
        array_to_string(key_validation_parts, E'\n'),
        array_to_string(key_new_parts, ', '),
        array_to_string(key_old_parts, ', '),
        array_to_string(data_new_parts, ', ')
    );

    EXECUTE command;
END;
$$;
