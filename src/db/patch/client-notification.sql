--@require row-event

perform public.create_row_event_trigger(
    'public',
    'client_row_event',
    'row-event',
    array['id'],
    array['name', 'salt', 'created_utc']
);

create trigger client_row_event_trg_all after
insert or update or delete
on public.client
for each row
execute procedure public.client_row_event()
;
