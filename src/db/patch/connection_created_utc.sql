--@require connection

ALTER table public.connection
add created_utc timestamp without time zone NOT NULL;
