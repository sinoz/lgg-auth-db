import delay from "delay";
import { second } from "msecs";
import net from "net";
import pg from "pg";
import test from "tape-promise/tape.js";

test("socket timeout", async t => {
    const client = new pg.Client({});

    try {
        await client.connect();

        let timedOut = false;
        const connection = (client as any).connection as pg.Connection;
        const socket = connection.stream as net.Socket;
        socket.on("timeout", () => timedOut = true);

        socket.setTimeout(2 * second);

        await delay(3 * second);

        t.equal(timedOut, true);
    }
    finally {
        await client.end();
    }
});

test("pg keepalive", async t => {
    const client = new pg.Client({});

    try {
        await client.connect();

        let timedOut = false;
        const connection = (client as any).connection as pg.Connection;
        const socket = connection.stream as net.Socket;
        socket.on("timeout", () => timedOut = true);

        socket.setTimeout(2 * second);

        await delay(1 * second);
        await client.query("");

        await delay(1 * second);
        await client.query("");

        await delay(1 * second);
        await client.query("");

        t.equal(timedOut, false);
    }
    finally {
        await client.end();
    }
});
