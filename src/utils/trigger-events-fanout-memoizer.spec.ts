import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";
import { getTriggerEventsFanoutMemoizer } from "./trigger-events-fanout-memoizer.js";

test("trigger-event", t => withContext(async context => {
    await initialize();

    const triggerEventsMemoizer = getTriggerEventsFanoutMemoizer(context.pool);

    const triggerEventsPromise = triggerEventsMemoizer.acquire("row-event");
    try {
        const triggerEvents = await triggerEventsPromise;
        triggerEvents.finished.catch(error => {
            if (abortController.signal.aborted) return;
            throw error;
        });

        const abortController = new AbortController();
        try {
            const triggerEventsFork = triggerEvents.fork(abortController.signal);

            await context.pool.query(`
insert into public.test (key, value)
values (1, 'one');
`);

            for await (const triggerEvent of triggerEventsFork) {
                assert(triggerEvent.op === "INSERT");

                t.deepEqual(triggerEvent.key, { key: 1 });
                t.deepEqual(triggerEvent.data, { value: "one" });

                abortController.abort();
            }
        }
        catch (error) {
            if (!abortController.signal.aborted) {
                throw error;
            }
        }
    }
    finally {
        triggerEventsMemoizer.release(triggerEventsPromise);
    }

    async function initialize() {
        await context.pool.query(`
create table public.test(
    key integer not null primary key,
    value text not null
);
select public.create_row_event_trigger(
    'public',
    'test_row_event',
    'row-event',
    array['key'],
    array['value']
);
create trigger test_trg_all before
insert or update or delete
on public.test
for each row
execute procedure public.test_row_event()
;
`);

    }

}));
