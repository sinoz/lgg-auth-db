import * as pg from "pg";
import { createBufferedAsyncIterable } from "query-source";

export async function listenChannel(
    client: pg.ClientBase,
    channel: string,
    signal: AbortSignal,
): Promise<AsyncIterable<string>> {
    const iterable = createBufferedAsyncIterable<string>();
    signal.addEventListener("abort", () => iterable.done());

    const onNotification = (notification: pg.Notification) => {
        if (notification.channel !== channel) return;

        iterable.push(notification.payload ?? "");
    };
    const onError = (error: unknown) => {
        iterable.error(error);
    };

    client.addListener("error", onError);
    client.addListener("notification", onNotification);

    await client.query(`LISTEN ${client.escapeIdentifier(channel)}`);

    return {
        async *[Symbol.asyncIterator]() {
            try {
                yield* iterable;
            }
            finally {
                await client.query(`UNLISTEN ${client.escapeIdentifier(channel)}`);

                client.removeListener("error", onError);
                client.removeListener("notification", onNotification);
            }
        },
    };

}
