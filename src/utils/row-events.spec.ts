import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";
import { createRowEvents } from "./row-events.js";
import { createTriggerEventsFanoutMemoizer } from "./trigger-events-fanout-memoizer.js";

test("row-event", t => withContext(async context => {
    await initialize();

    await context.pool.query(`
insert into public.test (key, value)
values (1, 'one');
`);

    const triggerEventsFanoutMemoizer = createTriggerEventsFanoutMemoizer(context.pool);

    const abortController = new AbortController();

    const rowEvents = createRowEvents({
        pool: context.pool, triggerEventsFanoutMemoizer,
        schema: "public", table: "test", channel: "row-event",
        filter: {},
        signal: abortController.signal,
    });
    const rowEventsIterator = rowEvents[Symbol.asyncIterator]();

    {
        const rowEventsNext = await rowEventsIterator.next();
        assert(rowEventsNext.done !== true);
        assert(rowEventsNext.value.type === "snapshot");

        t.deepEqual(
            rowEventsNext.value.rows,
            [
                {
                    key: 1,
                    value: "one",
                },
            ],
        );
    }

    await context.pool.query(`
insert into public.test (key, value)
values (2, 'two');
`);

    {
        const rowEventsNext = await rowEventsIterator.next();
        assert(rowEventsNext.done !== true);
        assert(rowEventsNext.value.type === "insert");

        t.deepEqual(
            rowEventsNext.value.key,
            {
                key: 2,
            },
        );
        t.deepEqual(
            rowEventsNext.value.data,
            {
                value: "two",
            },
        );
    }

    await context.pool.query(`
delete from public.test;
`);

    {
        const rowEventsNext = await rowEventsIterator.next();
        assert(rowEventsNext.done !== true);
        assert(rowEventsNext.value.type === "delete");

        t.deepEqual(
            rowEventsNext.value.key,
            {
                key: 1,
            },
        );
    }

    {
        const rowEventsNext = await rowEventsIterator.next();
        assert(rowEventsNext.done !== true);
        assert(rowEventsNext.value.type === "delete");

        t.deepEqual(
            rowEventsNext.value.key,
            {
                key: 2,
            },
        );
    }

    try {
        abortController.abort();

        await rowEventsIterator.next();
        t.fail();
    }
    catch (error) {
        assert(error instanceof Error);
        t.equal(error.name, "AbortError");
    }

    async function initialize() {
        await context.pool.query(`
create table public.test(
    key integer not null primary key,
    value text not null
);
select public.create_row_event_trigger(
    'public',
    'test_row_event',
    'row-event',
    array['key'],
    array['value']
);
create trigger test_trg_all before
insert or update or delete
on public.test
for each row
execute procedure public.test_row_event()
;
`);

    }

}));
