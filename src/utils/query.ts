import { setupCascadingAbort, waitForAbort } from "abort-tools";
import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import defer from "p-defer";
import { createAsyncIterableFanout, retryIterable } from "query-source";

export interface QueryFactoryOptions<S, E, A extends unknown[]> {
    initialState: S,
    reduce: (state: S, event: E) => S,
    settle: (state: S, event: E) => boolean,
    createSource: (signal: AbortSignal, ...args: A) => AsyncIterable<E>,
    calculateHash: (...args: A) => string,
    onError: (error: unknown) => void
    retryIntervalBase: number,
    retryIntervalCap: number,
}

export type QueryFactory<S, E, A extends unknown[]> = InstanceMemoizer<
    Promise<QuerySource<S, E>>, A
>

export function createQueryFactory<S, E, A extends unknown[]>(
    options: QueryFactoryOptions<S, E, A>,
): QueryFactory<S, E, A> {
    const {
        initialState, reduce, settle,
        createSource, calculateHash, onError,
        retryIntervalBase, retryIntervalCap,
    } = options;

    const abortControllers = new WeakMap<
        Promise<QuerySource<S, E>>,
        AbortController
    >();

    const createInstace = (...args: A) => {
        const abortController = new AbortController();
        const instance = instantiate(
            abortController.signal,
            ...args,
        );
        abortControllers.set(instance, abortController);
        abortController.signal.addEventListener(
            "abort",
            () => abortControllers.delete(instance),
        );
        return instance;
    };

    const destroyInstance = (
        instance: Promise<QuerySource<S, E>>,
    ) => {
        const abortController = abortControllers.get(instance);
        assert(abortController);
        abortController.abort();
    };

    const result = new InstanceMemoizer(
        createInstace,
        destroyInstance,
        calculateHash,
    );
    return result;

    async function instantiate(
        signal: AbortSignal,
        ...args: A
    ) {
        const deferred = defer<void>();
        let settled = false;

        let state = initialState;

        const source = retryIterable({
            signal,
            retryIntervalBase, retryIntervalCap,
            async * factory() {
                const abortController = new AbortController();
                setupCascadingAbort(signal, abortController);
                try {
                    const events = createSource(abortController.signal, ...args);
                    const reducedEvents = reduceEvents(events);
                    yield* reducedEvents;
                    await waitForAbort(signal, "aborted");
                }
                finally {
                    abortController.abort();
                }
            },
            // eslint-disable-next-line require-yield
            * mapError(error) {
                if (signal.aborted) return;

                onError(error);
            },
        });

        const fanout = createAsyncIterableFanout(source);
        const fork = fanout.fork;
        const getState = () => state;

        const querySource: QuerySource<S, E> = {
            getState,
            fork,
        };

        fanout.finished.catch(error => {
            // this will panic! but should never happen
            if (!signal.aborted) throw error;
        });

        await deferred.promise;

        return querySource;

        async function* reduceEvents(
            events: AsyncIterable<E>,
        ): AsyncIterable<QuerySourceElement<S, E>> {
            for await (const event of events) {
                state = reduce(state, event);

                yield { state, event };

                if (!settled && settle(state, event)) {
                    deferred.resolve();
                    settled = true;
                }
            }
        }
    }
}

export async function getQueryState<S, E, A extends unknown[]>(
    source: InstanceMemoizer<Promise<QuerySource<S, E>>, A>,
    args: A,
    linger: number,
): Promise<S> {
    const queryPromise = source.acquire(...args);
    try {
        const query = await queryPromise;
        const state = query.getState();
        return state;
    }
    finally {
        source.release(queryPromise, linger);
    }
}

export interface QuerySourceElement<S, E> {
    state: S
    event: E
}

export interface QuerySource<S, E> {
    fork(signal?: AbortSignal): AsyncIterable<QuerySourceElement<S, E>>;
    getState(): S;
}

export async function* mapQueryEvents<E>(
    iterable: AsyncIterable<QuerySourceElement<unknown, E>>,
): AsyncIterable<E> {
    for await (const element of iterable) {
        yield element.event;
    }
}
