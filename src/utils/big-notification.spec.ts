import assert from "assert";
import test from "tape-promise/tape.js";
import { withContext } from "../testing/index.js";
import { parseBigNotifications } from "./big-notification.js";
import { listenChannel } from "./channel.js";
import { withKeepAlive } from "./keep-alive.js";

test("big-notification", t => withContext(async context => {
    const channel = "ch-123";
    const message = "0123456789";

    const client = await context.pool.connect();
    try {
        const abortController = new AbortController();
        const notifications = await listenChannel(
            client, channel, abortController.signal,
        );
        const messagesWithKeepAlive = withKeepAlive(
            notifications,
            () => client.query(""),
        );
        const bigNotifications = parseBigNotifications(
            messagesWithKeepAlive,
        );

        const iterator = bigNotifications[Symbol.asyncIterator]();
        try {
            {
                const chunkSize = message.length;

                await context.pool.query(`
SELECT public.dispatch_big_notification($1, $2, $3);
`, [channel, message, chunkSize]);

                const next = await iterator.next();
                assert(next.done !== true);

                t.equal(next.value.payload, message);
            }

            {
                const chunkSize = message.length - 1;

                await context.pool.query(`
SELECT public.dispatch_big_notification($1, $2, $3);
`, [channel, message, chunkSize]);

                const next = await iterator.next();
                assert(next.done !== true);

                t.equal(next.value.payload, message);
            }

            {
                const chunkSize = 4;

                await context.pool.query(`
SELECT public.dispatch_big_notification($1, $2, $3);
`, [channel, message, chunkSize]);

                const next = await iterator.next();
                assert(next.done !== true);

                t.equal(next.value.payload, message);
            }
        }
        finally {
            abortController.abort();

            const next = await iterator.next();
            assert(next.done === true);
        }

        client.release();
    }
    catch (error) {
        client.release(true);
        throw error;
    }

}));
