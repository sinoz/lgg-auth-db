import assert from "assert";
import * as pg from "pg";
import { makeRowFilterFunction, makeRowFilterPg, RowFilter } from "table-access";
import { defaultSettings } from "../default-settings.js";
import { isAfterOffsetCycle } from "./cycle.js";
import { TriggerEventsFanoutMemoizer } from "./trigger-events-fanout-memoizer.js";

export interface RowSnapshotEvent<Row extends object> {
    type: "snapshot",
    rows: Row[]
}
export interface RowInsertRowEvent<Key extends object, Data extends object> {
    type: "insert",
    key: Key,
    data: Data,
}
export interface RowUpdateRowEvent<Key extends object, Data extends object> {
    type: "update",
    key: Key,
    data: Data,
}
export interface RowDeleteRowEvent<Key extends object> {
    type: "delete",
    key: Key,
}

export type RowEventUnion<Key extends object, Data extends object> =
    RowSnapshotEvent<Key & Data> |
    RowInsertRowEvent<Key, Data> |
    RowUpdateRowEvent<Key, Data> |
    RowDeleteRowEvent<Key>;

export interface RowEventOptions<Key extends object, Data extends object> {
    pool: pg.Pool,
    triggerEventsFanoutMemoizer: TriggerEventsFanoutMemoizer<Key, Data>,
    schema: string,
    table: string,
    channel: string,
    filter: RowFilter<Key> | Partial<Key>,
    wrapTreshold?: number,
    signal: AbortSignal,
}

export async function* createRowEvents<
    Key extends object, Data extends object
>(
    options: RowEventOptions<Key, Data>,
): AsyncIterable<RowEventUnion<Key, Data>> {
    const {
        pool, triggerEventsFanoutMemoizer,
        schema, table, channel,
        filter,
        wrapTreshold,
        signal,
    } = {
        wrapTreshold: defaultSettings.wrapTreshold,
        ...options,
    };

    const triggerEventsFanoutPromise = triggerEventsFanoutMemoizer.acquire(channel);
    try {
        let notificationOffset: number;

        // setup row events listener before snapshot
        const triggerEventsFanout = await triggerEventsFanoutPromise;
        const triggerEventsFork = triggerEventsFanout.fork(signal);

        // take a snapshot in a transaction, this will queue any updates
        // to the rows that we are querying until the transaction is complete
        const client = await pool.connect();
        try {
            await client.query("BEGIN TRANSACTION;");
            try {
                {
                    const result = await client.query(`
SELECT nextval('public.big_notification_seq') as n
;`);
                    assert(result.rowCount === 1, "expected one row");

                    const { rows } = result;
                    notificationOffset = rows[0].n;
                }

                {
                    const rowFilter = makeRowFilterPg(filter, "t");
                    const result = await client.query(`
SELECT row_to_json(t) AS r
FROM ${client.escapeIdentifier(schema)}.${client.escapeIdentifier(table)} AS t
${rowFilter.paramCount ? `WHERE ${rowFilter.filterSql}` : ""}
FOR SHARE
;`, rowFilter.param);

                    const { rows } = result;

                    yield {
                        type: "snapshot",
                        rows: rows.map(row => row.r as Key & Data),
                    };
                }
                await client.query("COMMIT TRANSACTION;");
            }
            catch (error) {
                await client.query("ROLLBACK TRANSACTION;");

                throw error;
            }

            client.release();
        }
        catch (error) {
            client.release(true);

            throw error;
        }

        // event row events
        const filterFunction = makeRowFilterFunction(filter);

        // start reading row events
        for await (const triggerEvent of triggerEventsFork) {
            if (!isAfterOffsetCycle(
                notificationOffset, triggerEvent.notification, wrapTreshold)
            ) continue;

            if (triggerEvent.schema !== schema) continue;
            if (triggerEvent.table !== table) continue;
            if (!filterFunction(triggerEvent.key)) continue;

            switch (triggerEvent.op) {
                case "INSERT":
                    yield {
                        type: "insert",
                        key: triggerEvent.key,
                        data: triggerEvent.data,
                    };
                    break;

                case "UPDATE":
                    yield {
                        type: "update",
                        key: triggerEvent.key,
                        data: triggerEvent.data,
                    };
                    break;

                case "DELETE":
                    yield {
                        type: "delete",
                        key: triggerEvent.key,
                    };
                    break;
            }
        }
    }
    finally {
        triggerEventsFanoutMemoizer.release(triggerEventsFanoutPromise);
    }
}
