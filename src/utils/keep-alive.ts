import { defaultSettings } from "../default-settings.js";

export async function* withKeepAlive<T>(
    source: AsyncIterable<T>,
    onKeepAlive: () => void,
    keepaliveInterval = defaultSettings.keepaliveInterval,
) {
    for await (const element of source) {
        const keepaliveTimer = setInterval(onKeepAlive, keepaliveInterval);
        try {
            yield element;
        }
        finally {
            clearInterval(keepaliveTimer);
        }
    }
}
