import pg from "pg";
import test from "tape-promise/tape.js";
import { listenChannel } from "./channel.js";

test("pg channel", async t => {
    const client = new pg.Client({});
    await client.connect();
    try {
        const controller = new AbortController();
        try {
            const channel = await listenChannel(client, "hello", controller.signal);

            await client.query("NOTIFY hello, 'hi'");

            for await (const message of channel) {
                t.equal(message, "hi");

                controller.abort();
            }
        }
        catch (error) {
            if (!controller.signal.aborted) {
                throw error;
            }
        }
    }
    finally {
        await client.end();
    }
});
