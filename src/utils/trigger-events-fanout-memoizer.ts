import assert from "assert";
import { InstanceMemoizer } from "instance-memoizer";
import * as pg from "pg";
import { AsyncIterableFanout, createAsyncIterableFanout } from "query-source";
import { defaultSettings } from "../default-settings.js";
import { parseBigNotifications } from "./big-notification.js";
import { listenChannel } from "./channel.js";
import { withKeepAlive } from "./keep-alive.js";

const cache = new WeakMap<pg.Pool, TriggerEventsFanoutMemoizer>();

export function getTriggerEventsFanoutMemoizer(
    pool: pg.Pool,
) {
    let cached = cache.get(pool);
    if (cached == null) {
        cached = createTriggerEventsFanoutMemoizer(pool);
        cache.set(pool, cached);
    }
    return cached;
}

export interface TriggerInsertEvent<Key extends object = any, Data extends object = any> {
    op: "INSERT";
    schema: string;
    table: string;
    key: Key;
    data: Data;
    notification: number;
}
export interface TriggerUpdateEvent<Key extends object = any, Data extends object = any> {
    op: "UPDATE";
    schema: string;
    table: string;
    key: Key;
    data: Data;
    notification: number;
}
export interface TriggerDeleteEvent<Key extends object = any> {
    op: "DELETE";
    schema: string;
    table: string;
    key: Key;
    notification: number;
}
export type TriggerEventUnion<Key extends object = any, Data extends object = any> =
    TriggerInsertEvent<Key, Data> |
    TriggerUpdateEvent<Key, Data> |
    TriggerDeleteEvent<Key>;

export type TriggerEventsFanout<Key extends object = any, Data extends object = any> =
    AsyncIterableFanout<TriggerEventUnion<Key, Data>>;

export type TriggerEventsFanoutMemoizer<Key extends object = any, Data extends object = any> =
    InstanceMemoizer<Promise<TriggerEventsFanout<Key, Data>>, [string]>;

export function createTriggerEventsFanoutMemoizer<
    Key extends object = any, Data extends object = any
>(
    pool: pg.Pool,
    keepaliveInterval = defaultSettings.keepaliveInterval,
    maxChunkAge = defaultSettings.maxChunkAge,
): TriggerEventsFanoutMemoizer<Key, Data> {
    const abortControllers = new WeakMap<
        Promise<AsyncIterableFanout<TriggerEventUnion<Key, Data>>>, AbortController
    >();
    const createInstace = (
        channel: string,
    ) => {
        const abortController = new AbortController();
        const { signal } = abortController;

        const instance = createInstancePromise(channel, signal);
        abortControllers.set(instance, abortController);
        abortController.signal.addEventListener(
            "abort",
            () => abortControllers.delete(instance),
        );
        return instance;
    };

    const destroyInstance = (
        instance: Promise<AsyncIterableFanout<TriggerEventUnion<Key, Data>>>,
    ) => {
        const abortController = abortControllers.get(instance);
        assert(abortController);
        abortController.abort();
    };

    const memoizer = new InstanceMemoizer(
        createInstace,
        destroyInstance,
        channel => channel,
    );

    return memoizer;

    async function createInstancePromise(
        channel: string,
        signal: AbortSignal,
    ): Promise<TriggerEventsFanout<Key, Data>> {
        const client = await pool.connect();

        let notifications: AsyncIterable<string>;
        notifications = await listenChannel(client, channel, signal);
        notifications = withKeepAlive(
            notifications,
            () => client.query(""),
            keepaliveInterval,
        );
        const bigNotifications = parseBigNotifications(
            notifications,
            maxChunkAge,
        );

        const events = {
            async *[Symbol.asyncIterator]() {
                try {
                    for await (const { key, payload } of bigNotifications) {
                        const parsedPayload = JSON.parse(payload);
                        yield {
                            ...parsedPayload,
                            notification: key,
                        };
                    }
                    client.release();
                }
                catch (error) {
                    client.release(true);
                }
            },
        };

        const fanout = createAsyncIterableFanout(events);
        return fanout;
    }

}
